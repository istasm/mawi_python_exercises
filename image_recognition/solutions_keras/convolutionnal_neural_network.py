from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten
from tensorflow.keras.layers import Dense, Dropout, Input

# Define the CNN model

def make_convolutionnal_nn(
    output_size,
    kernel_size,
    neurons,
    pool_size,
    activation_function,
    dropout_prob=0.2
):


    CNN_model = Sequential([
        Input((pixel_number, pixel_number, 1)),

        Conv2D(neurons, kernel_size, activation=activation_function),
        MaxPooling2D(pool_size),
        Dropout(dropout_prob),

        Conv2D(neurons*2, kernel_size, activation=activation_function),
        MaxPooling2D(pool_size),
        Dropout(dropout_prob),

        Flatten(),
        Dense(neurons*4, activation=activation_function),
        Dropout(dropout_prob),

        Dense(output_size, activation='softmax')
    ])
    return CNN_model

pixel_number = 28

activation_function = 'relu' # choose relu again
# choose the kernel size and the pooling size
kernel_size = (3, 3)
pool_size = (2, 2)

output_size = 10
neurons = 8

CNN_model = make_convolutionnal_nn(
    output_size,
    kernel_size,
    neurons,
    pool_size,
    activation_function,
    dropout_prob=0.2
)

CNN_model.summary()