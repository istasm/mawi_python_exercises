
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout


# Define the MLP model with ReLU and Dropout
def make_multi_layers(
    input_size,
    output_size,
    hidden_neurons,
    activation_function,
    dropout_prob=0.25
    ):
    model = Sequential([
        Dense(hidden_neurons[0], activation=activation_function, input_shape=(input_size,)),
        Dropout(dropout_prob),
        Dense(hidden_neurons[1], activation=activation_function, input_shape=(input_size,)),
        Dropout(dropout_prob),
        Dense(hidden_neurons[2], activation=activation_function, input_shape=(input_size,)),
        Dropout(dropout_prob),
        Dense(output_size, activation='softmax')
    ])
    return model


# choose a number of neurons for each layer. 
neurons = [64, 128, 64]

pixel_number = 28
output_size = 10
activation_function = 'sigmoid' # let's have sigmoid for a change

# Instantiate the model
multi_layers = make_multi_layers(pixel_number**2, output_size, neurons, activation_function)

