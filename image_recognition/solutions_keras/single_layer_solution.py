from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense


# Define the MLP model with ReLU and Dropout
def define_single_layer(input_size, output_size, hidden_neurons, activation_function):
    model = Sequential([
        Dense(hidden_neurons, activation=activation_function, input_shape=(input_size,)),
        Dense(output_size, activation='softmax')
    ])
    return model


# Define the number of neurons for the hidden layer, typically a power of 2
num_neurons = 64

# Create an instance of the custom neural network
pixel_number = 28 # pixel_size size,
output_size = 10  # output size
activation_function = 'relu' # standard choices are relu, sigmoid, or tanh

# reshape the data to 1-dimensional vector
x_train, x_test = x_train.reshape(-1, pixel_number**2), x_test.reshape(-1, pixel_number**2)

# Instantiate the model
single_layer = define_single_layer(pixel_number**2, output_size, num_neurons, activation_function)

# change target from number to one-hot encoding
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)

# Define a batch size
batch_size = 32