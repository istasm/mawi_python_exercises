from tensorflow import keras

#### Define loss functions and optimizer

# specify loss function, cross entropy is the standard choice here
loss_function = keras.losses.SparseCategoricalCrossentropy()

# specify optimizer and learning rate
# a strong learning rate means the weights are updated a lot at each iteration, 
# however, a too big learning rate will make you miss a narrow minimum
# If you see strong oscillations in error, learning rate is too big
# if nothing changes in a few epochs learning rate is too small
learning_rate = 0.01
optimizer_single_layer = keras.optimizers.Adam(learning_rate=learning_rate) # find optimizers in keras.optimizers., typically Adam or SGD

# The number of epochs is the number of times all the dataset is evaluated by the model
epochs = 10