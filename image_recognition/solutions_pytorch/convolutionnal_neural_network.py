import torch
import torch.nn as nn
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

class SimpleCNN(nn.Module):
    def __init__(self, output_size, in_channels, kernel_size, 
                 out_channels, dropout_prob=0.2):
        super(SimpleCNN, self).__init__()
        pixel_size = 28 # change this value for other images
        # This padding value is probably not the optimal value,
        # but allows to have simple expression for the dimensions
        # of the layers
        padding = kernel_size // 2

        # we take the architecture to be first a layer with
        # a convolution, activation and pooling
        self.conv1 = nn.Conv2d(in_channels, out_channels, kernel_size=kernel_size, stride=1, padding=padding)
        # output: (28 - kernel_size + 1) + 2 * padding
        self.relu1 = nn.ReLU()
        # the most common pooling function is max pooling, which take the maximum value 
        # in a square defined by the kernel size
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.dropout1 = nn.Dropout(dropout_prob)

        self.conv2 = nn.Conv2d(out_channels, out_channels, kernel_size=kernel_size, stride=1, padding=padding)
        self.relu2 = nn.ReLU()
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.dropout2 = nn.Dropout(dropout_prob)

        # Now the input must be flatten to be in the last two layers
        self.flatten = nn.Flatten()
        ### careful! The dimension of this layer might be incorrect if:
        # - you change the kernel size in maxpool2d
        # - you change the padding
        self.fc1 = nn.Linear(out_channels * (pixel_size // 4)**2, 128)
        self.relu3 = nn.ReLU()
        self.fc2 = nn.Linear(128, output_size)

    def forward(self, x):
        # Important, note that the image is not flatten, unlike with the previous models
        # we keep the structure of the 2D image intact, which allows to keep
        # spatial information.
        x = self.conv1(x)
        x = self.relu1(x)
        x = self.pool1(x)
        x = self.dropout1(x)

        x = self.conv2(x)
        x = self.relu2(x)
        x = self.pool2(x)
        x = self.dropout2(x)

        # only here, after several layers we flatten the array
        x = self.flatten(x)
        x = self.fc1(x)
        x = self.relu3(x)
        x = self.fc2(x)

        return x

activation_function = torch.relu # choose relu again
# number of channels (i.e. colors) going in
conv_in_channels = 1
# define the number of learnable kernels
conv_out_channels = 10
# choose the kernel size, 3 is the minimum, can be rectangular
kernel_size = 5
output_size = 10

# Several_layers = Several_layer_perceptron(pixel_size, neurons, output_size, activation_function)
CNN_model = SimpleCNN(output_size, conv_in_channels, kernel_size,
                      conv_out_channels).to(device)