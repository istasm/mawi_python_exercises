import torch
import torch.nn as nn

# We now define a model with several layers and dropout
class Multi_layers(nn.Module):
    def __init__(self, input_size, hidden_sizes, output_size,
                 activation_function, dropout_prob=0.25):
        super(Multi_layers, self).__init__()
        
        self.input_size = input_size
        
        self.hidden_layer_1 = nn.Linear(input_size*input_size, hidden_sizes[0])
        self.relu1 = activation_function
        self.dropout1 = nn.Dropout(dropout_prob)

        self.hidden_layer_2 = nn.Linear(hidden_sizes[0], hidden_sizes[1])
        self.relu2 = activation_function
        self.dropout2 = nn.Dropout(dropout_prob)

        self.hidden_layer_3 = nn.Linear(hidden_sizes[1], hidden_sizes[2])
        self.relu3 = activation_function
        self.dropout3 = nn.Dropout(dropout_prob)

        self.output_layer = nn.Linear(hidden_sizes[2], output_size)

    # Define the function, given an image as entry, should return
    # a probability for each label 
    def forward(self, x):
        x = x.reshape(-1, 1 * self.input_size * self.input_size)
        
        # every output is called x to make things clearer
        # remember that each x may have a different shape
        # from layer to layer.
        
        # Pass the input through the hidden layer and 
        # - apply the linear transformation w x + b
        # - apply the non linear activation function
        # - apply the dropout regularization
        x = self.hidden_layer_1(x)
        x = self.relu1(x)
        x = self.dropout1(x)

        x = self.hidden_layer_2(x)
        x = self.relu2(x)
        x = self.dropout2(x)

        x = self.hidden_layer_3(x)
        x = self.relu3(x)
        x = self.dropout3(x)
        
        x = self.output_layer(x)

        return x

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# choose a number of neurons for each layer
pixel_size = 28
output_size = 10
neurons = [64, 32, 32]
activation_function = torch.relu # choose relu for a change
# Several_layers = Several_layer_perceptron(pixel_size, neurons, output_size, activation_function)
multi_layers = Multi_layers(pixel_size, neurons, output_size,
                            activation_function, dropout_prob=0.2).to(device)

# Print the number of parameters in each layer
for name, param in multi_layers.named_parameters():
    print(f"Layer: {name}, Parameters: {param.numel()}")