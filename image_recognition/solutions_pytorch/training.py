import torch
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def train_model(num_epochs, model, train_loader, test_loader,
                criterion, optimizer, batch_size, save_name=''):
    """
    perform the training of the model with the data in train_loader.
    Save the models as a file .pt at all epochs in the notebook folder.
    
    epochs: integer, training ends when model saw the data for
            the number of epochs
    model: the neural network, a pytorch model
    train_loader: an iterable over the dataset that contains data and labels
    optimizer: an optimizer (SGD, Adam...) from torch.optim
    """
    model.to(device)
    save_path = '.'

    model_names = [] #names of the saved models
    for epoch in range(num_epochs):
        model.train()
        
        train_loss = 0 # monitor loss
        for images, labels in train_loader:
            images, labels = images.to(device), labels.to(device)

            # pitfall, you have to clear the gradients at each step
            # Otherwise, pytorch accumulate gradients (which is useful for other architectures like RNN)    
            optimizer.zero_grad()
            # forward pass: compute predicted outputs \hat{y} by passing inputs x_train to the model
            outputs = model(images)
            # calculate the loss
            loss = criterion(outputs, labels)
            # backward pass: compute gradient of the loss with respect to model parameters
            loss.backward()
            # update the parameters
            optimizer.step()

            # keep track of loss
            train_loss += loss.item()
        train_loss /= (len(train_loader) * batch_size)

        # Validation
        model.eval()
        total_correct = 0
        test_loss = 0

        # the no_grad can be used when testing to make it more efficient, but it is not necessary
        with torch.no_grad():
            for images, labels in test_loader:
                images, labels = images.to(device), labels.to(device)
                outputs = model(images)
                # output is a vector of probabilities
                # it consists in 9 values between 
                # 0 and 1 with hopefully on value close to 1.
                # we choose the max
                # value as the most likely digit.
                _, predictions = torch.max(outputs, 1)
                total_correct += (predictions == labels).sum().item()

                # we monitor the test loss
                test_loss += criterion(outputs, labels).item()

        # normalize by number of samples in test
        accuracy = 100 * total_correct / (len(test_loader) * batch_size)
        test_loss /= (len(test_loader) * batch_size)
        print(f'Epoch {epoch+1}/{num_epochs}, train loss: {train_loss:.4f}', end=" ")
        print(f'test loss {test_loss}, test Accuracy: {accuracy:.4f}')

        # Save model checkpoint after each epoch
        checkpoint_path = f'{save_path}/{save_name}_{epoch+1}.pt'
        model_names.append(checkpoint_path)
        torch.save({
            'epoch': epoch + 1,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'train_loss': train_loss,
            'test_loss': test_loss,
            'accuracy': accuracy,
        }, checkpoint_path)
    print('Training complete.')
    return model_names

